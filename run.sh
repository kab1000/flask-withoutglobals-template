#!/usr/bin/env bash

# for use database placed on your disk, it is need to add the option to docker run:
# -v <path to your database directory>>:/app/database
docker run --rm --net=host -P --name "rest"  rest
