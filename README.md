###   Flask withoyt globals template
### Используется Flask, sqllite в качестве базы данных
### Реализован REST сервер

## Структура приложения:
```
root_of_application
   -- Dockerfile
   -- run.sh
   -- build.sh
   -- app
       -- api              - эндпойнты апи
       -- database         - каталог базы дынных
       -- db               - модули работы с бд
       -- flask_tools      - модули для инициализации Flask
       -- tests            - тесты
       start_app.py        - точка входа в приложения
       refister_routes.py
       settings.py
```

## RUn tests
```
cd app
pytest tests
```

## Построение Docker image
```
Из корневого каталога (root_of_application)
bash build.sh
Либо из коренвого каталога запустить в консоли:

DOCKER_BUILDKIT=1 docker build --file  ./Dockerfile --force-rm -t "latest"  -t "rest" .
```

## Старт приложения в докер-контейнере
```
bash run.sh 
1. В исходном виде запускает докер-контейнер. Используется бд врутри контейнера. 
ВНИМАНИЕ!!! бд будет потеряна после остановки контейнера

2. Для использования постоянной базы данных модифицировать run.sh,
добавив опцию -v <path to your database directory>>:/app/database
По умолчанию именем базы данных является database.db
Для изменения имени бд, добавить опцию -e DB_URL=app/database/<name_od_db_file>

3. Либо стартовать докер контейнер из консоли:
$ docker run --rm --net=host -P 
-v <path_to_db_directory>:/app/database 
-e DB_URL=app/database/<name_of_db_file> --name "rest"  rest

4. Остановка контейнера.
docker stop rest

5. Приложение в качестве WSGI сервера использует gunicorn.
Вы можете добавить.изменить опции gunicorn по умолчанию, 
добавив их в docker run ...... rest <gunicorn options here>
```

### Отправка тестовых запросов
```
curl -i -d '{"login": "user1", "password": "12345678"}' \
http://127.0.0.1:5000/login/register

curl -i -d '{"login": "user2", "password": "12345678"}' \
http://127.0.0.1:5000/login/register

curl -i -d '{"login": "user2", "password": "12345678"}' \
http://127.0.0.1:5000/login/login

curl -i -d '{"login": "user1", "password": "12345678"}' \
http://127.0.0.1:5000/login/login

```
