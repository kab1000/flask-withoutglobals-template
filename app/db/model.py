import dataclasses


@dataclasses.dataclass
class User:
    login: str
    password: str
