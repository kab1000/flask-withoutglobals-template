import sqlite3
import sys

from flask import Flask, g, request
from app.db.get_db_conn import get_db


def register_db_conn_handlers(app: Flask):
    @app.before_request
    def open_cursor():
        """get database cursor"""
        try:
            conn: sqlite3.Connection = get_db()
            cursor = conn.cursor()
            g.cursor = cursor
            g.db_connect = conn
        except Exception as e:
            print("BEFORE REQUEST db:", e, file=sys.stderr)
            return
        request.cursor = cursor
        request.db_connect = conn

    @app.teardown_request
    def close_cursor(exception):
        """close current request cursor"""
        if not hasattr(g, "cursor"):
            return
        try:
            cursor: sqlite3.Cursor = g.cursor
            cursor.close()
        except Exception as e:
            print("ERROR request_teardown:", e, file=sys.stderr)

    @app.before_first_request
    def check_shema():
        """Check if tables exists/ If not - create them"""
        db = get_db()
        q = "SELECT name FROM sqlite_master WHERE type='table' and name='user';"
        q_create = "CREATE TABLE IF NOT EXISTS user (id PRIMARY KEY, login text, password text);"
        q_index = "CREATE UNIQUE INDEX IF NOT EXISTS user_login ON user(login);"

        r = db.execute(q)
        res = r.fetchall()

        if not res:
            print("Database not exists. Create DATABASE", file=sys.stderr)
            db.execute(q_create)
            db.execute(q_index)
            db.commit()
