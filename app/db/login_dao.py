import dataclasses
import hashlib
import sqlite3
from typing import Tuple, Dict, Union

from app.db.model import User


def cript_password(password: str) -> str:
    return hashlib.sha256(bytes(password, "utf-8")).hexdigest()


def create_user(cursor: sqlite3.Cursor, login: str, password: str) -> Tuple[bool, str]:
    """Create User in database"""
    query = "INSERT INTO user (login, password) VALUES (?, ?);"
    found, user = get_user(cursor, login, password)
    password = cript_password(password)     # minimal defense of password

    if found:
        return False, "User with given login, password already exist"
    try:
        result = cursor.execute(query, (login, password))
        cursor.connection.commit()
        if result.rowcount != 1:
            raise sqlite3.DatabaseError
    except Exception as e:
        return False, f"Database error create user {e} "

    return True, ""


def get_user(cursor: sqlite3.Cursor, login: str, password: str) -> Tuple[bool, Union[Dict, str]]:
    """Get user from database. Check password. If not valid return error."""
    query = "SELECT login, password FROM user WHERE login=:login;"
    result = cursor.execute(query, (login,)).fetchone()
    if result:
        user = User(**result)
        if user.password != cript_password(password):
            return False, "Invalid password"
        return True, dataclasses.asdict(user)

    return False, "User not found"
