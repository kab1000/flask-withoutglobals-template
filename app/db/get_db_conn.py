import sqlite3
from flask import g, current_app


def get_db() -> sqlite3.Connection:
    url = current_app.config["DB_URL"]
    db = getattr(g, 'db_connect', None)
    if db is None:
        db = g.db_connect = sqlite3.connect(url)
        db.row_factory = sqlite3.Row
    return db
