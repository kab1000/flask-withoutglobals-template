from app.db.get_db_conn import get_db


def test_db(client):
    """Test if tables and keys exists"""
    q = "SELECT * FROM sqlite_master WHERE tbl_name=?;"  # type='table' AND
    r = client.get("/api/")   # datbase tables check before first request. If need - created

    db = get_db()

    r = db.execute(q, ('user', ))
    res = [{"name": e["name"], "type": e["type"], "tbl_name": e["tbl_name"]} for e in r.fetchall()]
    # check that table, indexes exists in db
    assert res[0]["name"] == "user"
    assert res[1]["name"] == "sqlite_autoindex_user_1" and res[1]["type"] == "index"
    assert res[2]["name"] == "user_login" and res[2]["type"] == "index"
