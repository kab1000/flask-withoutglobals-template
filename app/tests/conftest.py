import pytest

from app.flask_tools.create_app import create_app
from app.settings import AppSettings


class TestSettings(AppSettings):
    def __init__(self):
        super().__init__()
        self.DB_URL = ":memory:"


settings = TestSettings()


@pytest.fixture(scope='module')
def client():

    flask_app = create_app(__name__, settings)

    flask_app.testing = True
    testing_client = flask_app.test_client()

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()
