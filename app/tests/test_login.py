import pytest

from app.db.get_db_conn import get_db
from app.db.login_dao import create_user, cript_password


def test_index(client):
    r = client.get("/api/")
    assert r.status_code == 200
    assert r.get_json() == {'200': 'connect to server'}


@pytest.fixture()
def clear_db():
    """Delete test records from db"""
    test_data = {"login": "12345", "password": "12345678"}
    yield
    db = get_db()
    q = "DELETE FROM user WHERE login=?;"
    db.execute(q, (test_data["login"], ))


@pytest.fixture()
def clear_db_before():
    """Delete test records from db"""
    test_data = {"login": "12345", "password": "12345678"}
    db = get_db()
    q = "DELETE FROM user WHERE login=?;"
    db.execute(q, (test_data["login"], ))
    yield


@pytest.mark.parametrize("test_data, status_code, error_msg",
                         [
                             [{"login": "12345", "password": "12345678"}, 200,
                              b'{"200":"register success"}\n'],
                             # sorry, dut this test depends on previoue
                             [{"login": "12345", "password": "12345678"}, 400,
                              b'400 Bad Request: User with given login, password already exist'],
                             [{"login": "1234", "password": "12345678"}, 400,
                              b'400 Bad Request: login must has 5 or more symbols'],
                             [{"login": "123456", "password": "1234567"}, 400,
                              b'400 Bad Request: password must has 8 or more symbols'],
                             [{"login": "1234", "password": "12345678-"},400,
                              b'400 Bad Request: login must has 5 or more symbols,'
                              b' password must has only letters and digits'],
                         ]
                         )
def test_create(client,  test_data: dict[str, str], status_code: int, error_msg: bytes):
    """Test login/register endpoint"""

    r = client.post("/login/register", data=test_data, follow_redirects=True)

    assert r.status_code == status_code
    assert r.data == error_msg

    if status_code == 200:
        conn = get_db()
        r = conn.execute("SELECT login, password FROM user WHERE login = :1;", (test_data["login"], ))
        r = r.fetchone()
        assert r["login"] == test_data["login"]
        assert r["password"] == cript_password(test_data["password"])


@pytest.fixture()
def user_data(client, clear_db_before):
    def _create(login: str, password: str):
        conn = get_db()
        r = create_user(conn.cursor(), login, password)
        assert r[0], r[1]

    return _create


def test_login(client, user_data):
    """Test login/login endpoint"""
    test_data = {"login": "12345", "password": "12345678"}
    cripted = test_data.copy()
    cripted["password"] = cript_password(test_data["password"])

    user_data(*list(test_data.values()))

    r = client.post("/login/login", data=test_data, follow_redirects=True)
    assert r.status_code == 200
    assert r.get_json() == cripted  # test_data

    invalid_data = test_data.copy()
    invalid_data["login"] = test_data["login"] + "1"
    r = client.post("/login/login", data=invalid_data, follow_redirects=True)

    assert r.status_code == 400
    assert r.data == b'400 Bad Request: User not found'
