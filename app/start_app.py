from app.flask_tools.create_app import create_app
from app.settings import settings

app = create_app(__name__.split('.')[0], settings)

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000, debug=True)
