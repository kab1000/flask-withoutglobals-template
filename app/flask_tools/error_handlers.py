import werkzeug.exceptions
from flask import Flask


def register_error_handlers(app: Flask):
    @app.errorhandler(werkzeug.exceptions.BadRequest)
    def handle_bad_request(e):
        return 'bad request! ' if not str(e) else str(e), 400

    @app.errorhandler(werkzeug.exceptions.NotFound)
    def handle_bad_route(e):
        return 'api method not found!' if not str(e) else str(e), 404

    @app.errorhandler(werkzeug.exceptions.MethodNotAllowed)
    def handle_bad_method(e):
        return 'api method not allowed!' if not str(e) else str(e), 405

    @app.errorhandler(werkzeug.exceptions.InternalServerError)
    def handle_server_error(e):
        return 'Internal server error' if not str(e) else str(e), 500

    app.register_error_handler(400, handle_bad_request)
    app.register_error_handler(404, handle_bad_route)
    app.register_error_handler(405, handle_bad_method)
    app.register_error_handler(500, handle_server_error)
