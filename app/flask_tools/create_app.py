from flask import Flask

from app.db.handle_conn import register_db_conn_handlers
from app.flask_tools.error_handlers import register_error_handlers
from app.register_routes import register_routes
from app.settings import AppSettings


def create_app(name: str, settings: AppSettings) -> Flask:
    """Create flask application. Register handlers and routes."""
    app = Flask(name)
    app.config["DB_URL"] = settings.DB_URL

    register_error_handlers(app)

    register_routes(app)

    register_db_conn_handlers(app)

    return app
