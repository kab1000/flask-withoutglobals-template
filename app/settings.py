import os


class AppSettings:
    def __init__(self):
        self.DB_URL = os.environ.get("DB_URL", "database//database.db")    # ":memory:"


settings = AppSettings()
