from flask import Flask
from app.api.index import bp as index_bp
from app.api.login_api import bp as login_bp


def register_routes(app: Flask):
    app.register_blueprint(index_bp)
    app.register_blueprint(login_bp)
