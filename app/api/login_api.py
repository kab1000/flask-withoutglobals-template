import sys

import werkzeug.exceptions
from werkzeug.datastructures import MultiDict
from wtforms import Form, StringField, validators, Field, ValidationError
from flask import Blueprint, request, abort, g

from app.db.login_dao import create_user, get_user

bp = Blueprint('login', __name__, url_prefix="/login")


def validate_isalnum(form: Form, data: Field):  # noqa
    # print("............login outer", data.data, data.name)
    field_data: str = data.data
    if not field_data.isalnum():
        raise ValidationError(' must has only letters and digits')
    return True


class LoginForm(Form):
    login = StringField(
        "login",
        validators=[
            validators.DataRequired(message=" is required"),
            validators.length(min=5, message=" must has 5 or more symbols"),
            validate_isalnum
        ])
    password = StringField("password", validators=[
        validators.DataRequired(message=" is required"),
        validators.length(min=8, message=" must has 8 or more symbols"),
        validate_isalnum
    ])


@bp.route("/register", methods=['POST'])
def register_user():
    if request.method != "POST":
        abort(405, "Only POST method allowed")

    # print(">>>>", request.form, request.json, request.args)
    # print("header>>", request.headers)
    form = LoginForm(formdata=request.form or MultiDict(request.json))

    if not form.validate():
        error = ", ".join([kv+"".join(ers) for kv, ers in form.errors.items()])
        abort(400, error)
    else:
        login = form.login.data
        password = form.password.data

    result = ["", "Server error"]
    try:
        cursor = getattr(g, "cursor")
        result = create_user(cursor, login, password)   # noqa
    except werkzeug.exceptions.BadRequest as e:
        abort(400, str(e))
    except Exception as e:
        print("register endpoint  Error>>>>", e, file=sys.stderr)
        abort(500, str(e))

    if not result[0]:
        abort(400, result[1])

    return {200: "register success"}


@bp.route("/login", methods=['POST'])
def login():
    if request.method != "POST":
        abort(405)

    form = LoginForm(formdata=request.form or MultiDict(request.json))

    if not form.validate():
        error = ", ".join([kv+"".join(ers) for kv, ers in form.errors.items()])
        abort(400, error)
    else:
        login = form.login.data
        password = form.password.data

    success, user = get_user(g.cursor, login, password)     # noqa
    if not success:
        abort(400, user)

    return user
