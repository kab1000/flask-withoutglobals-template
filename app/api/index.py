from flask import Blueprint

bp = Blueprint('index', __name__, url_prefix='/api')


@bp.route("/", methods=["GET"])
def index():
    return {200: "connect to server"}
