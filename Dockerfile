FROM python:3.9 AS compile-image

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /

COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install -r requirements.txt  &&  \
    mkdir -v app

COPY app/ app/

FROM python:3.9-slim AS build-image

ENV PATH="/opt/venv/bin:$PATH"
ENV PYTHONPATH="${PYTHONPATH}:/app"

COPY --from=compile-image /opt/venv /opt/venv
COPY --from=compile-image /app /app

RUN chmod -R 777 app

# If not connect volume use image internal db. Lost after container stop
ENV DB_URL=app/database/database.db

ENTRYPOINT ["gunicorn", "app.start_app:app"]
CMD ["--workers=2", "--bind=:8000", "--worker-class=sync"]
